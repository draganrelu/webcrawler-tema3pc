build:
	g++ -Wall -g -std=c++11 source/server.cpp source/serverCommandExecutor.cpp source/exitError.cpp source/msg.cpp source/fileReceiver.cpp -o server
	g++ -Wall -g -std=c++11 source/client.cpp source/exitError.cpp source/msg.cpp source/pageDownloader.cpp source/fileSender.cpp -o client
clean:
	rm -f client server *.stdout *.stderr
