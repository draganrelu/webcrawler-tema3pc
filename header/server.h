#ifndef _SERVER_H_
#define _SERVER_H_
#include <fcntl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <string.h>

void exitError(const char* text);
void parseArguments(int argc, char** argv);
void initServerInfo();
void openServerSocket();
void setAddress();
void bindAddress();
void listenClients();
void startServer();
void setDescriptors();
void runServer();
int main(int argc, char** argv);

#endif
