#ifndef _CLIENT_H_
#define _CLIENT_H_
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
void exitError(const char* text);
void parseArguments(int argc, char** argv);
void openServerSocket();
void setServerAddress();
void connectToServer();
void setDescriptors();
void initClientInfo();
void startClient();
void runClient();
void closeClient();
int main(int argc, char** argv);
#endif
