#ifndef _FILE_SENDER_H_
#define _FILE_SENDER_H_
#include <string>
#include <vector>
#include "clientInfo.h"
void sendFile(ClientInfo& cInfo, const std::string& filename, const std::string& tmpFileName, const std::string& domain, const char& depth, const bool& recursive, const bool& everything);
void sendLinksInPage(const ClientInfo& cInfo, const std::string& tmpFileName,  const std::string& domain, const char& depth, const bool& everything);
void readHeader(FILE* f);
void getFiles(std::vector<std::string>& files, const std::string& tmpFileName, const bool& everything);
void getFiles(std::vector<std::string>& files, const bool& everything, const std::string& line);
void sendLinks(const ClientInfo& cInfo, const std::string& domain, const char& depth, const std::vector<std::string>& links);
bool checkLinkOrigin(const std::string& link);
bool checkHtmlLink(const std::string& link);
bool checkIsFile(const std::string& link);


#endif
