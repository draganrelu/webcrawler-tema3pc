#ifndef _SERVER_COMMAND_EXECUTOR_H_

#define _SERVER_COMMAND_EXECUTOR_H_
#include "serverInfo.h"
void executeCommand(ServerInfo& sInfo);
void executeStatus(ServerInfo& sInfo);
void executeDownload(ServerInfo& sInfo);
void executeExit(ServerInfo& sInfo);
#endif
