#ifndef _FILE_RECEIVER_H_
#define _FILE_RECEIVER_H_
#include "msg.h"
#include <string>

int openFile(const std::string& path);
void receiveFile(msg& m);


#endif
