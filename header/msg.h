#ifndef _MSG_H_
#define _MSG_H_
#include "constants.h"

typedef enum
{
    COMMAND,
    SHUTDOWN,
    LINK,
    CALLBACK,
    FILEPACK

} msgType;

typedef struct{
    msgType type;
    int len;
    char payload[PAYLOAD_LEN];
} msg;

void sendMsg(msg* m, int sd);
void recvMsg(msg* m, int sd);

#endif
