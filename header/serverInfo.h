#ifndef _SERVER_INFO_
#define _SERVER_INFO_
#include <unordered_set>
#include <unordered_map>
#include <string>
#include <queue>
#include "constants.h"
#include <netinet/in.h>
#include <set>
#include <stdio.h>
#include <stdlib.h>
struct ServerInfo
{
    std::unordered_map<int, int> connectedClients;
    std::unordered_map<int,int> jobsDone;
    std::set<std::pair<int,int>> idleClients;
    std::queue<std::pair<std::string,char>> queuedLinks;

    bool recursive;
    bool everything;
    bool logging;
    bool running;
    std::string logFileName;
    FILE* logFileOut;
    FILE* logFileErr;

    int port;
    fd_set read_fds;
    fd_set tmp_fds;
    int fdmax;
    int serverSocket;
    struct sockaddr_in addr;
    char buffer[BUFLEN];
};


#endif
