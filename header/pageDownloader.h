#ifndef _PAGE_DOWNLOADER_H_
#define _PAGE_DOWNLOADER_H_
#include "clientInfo.h"
#include "msg.h"
#include <string>
void downloadPage(const ClientInfo& cInfo, const msg& m, std::string& filePath, std::string& tmpFileName, std::string& domain, char& depth, bool& recursive, bool& everything);
int connectToHttpServer(const ClientInfo& cInfo, const std::string& domain);
void sendGetCommand(const ClientInfo& cInfo, int serverSocket, const std::string& filePath);
void getFile(const ClientInfo& cInfo, int serverSocket, FILE* tmpfile);
void getTokens(const char* link, std::string& hostname, std::string& curPath, std::string& filePath);

#endif
