#ifndef _CLIENT_INFO_H_
#define _CLIENT_INFO_H_
#include <netinet/in.h>
#include <string>
#include "constants.h"
struct ClientInfo
{
    int portServer;
    std::string ipServer;
    bool logging;
    std::string logFileName;
    FILE* logFileOut;
    FILE* logFileErr;
    int serverSocket;
    struct sockaddr_in serverAddr;
    fd_set read_fds;
    fd_set tmp_fds;
    bool running;
    char buffer[BUFLEN];
};
#endif
