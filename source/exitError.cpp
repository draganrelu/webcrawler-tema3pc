#include <iostream>
using namespace std;
void exitError(const char* text)
{
    cerr << text << "\n";
    exit(1);
}
