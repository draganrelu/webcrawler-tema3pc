#include "../header/msg.h"
#include "../header/constants.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void sendMsg(msg* m, int sd)
{
    char buf[BUFLEN];
    memcpy(buf, m, sizeof(msg));
    send(sd, buf, BUFLEN, 0);
}

void recvMsg(msg* m, int sd)
{
    char buf[BUFLEN];
    recv(sd, buf, BUFLEN, 0);
    memcpy(m, buf, sizeof(msg));
}

