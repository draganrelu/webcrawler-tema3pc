#include <iostream>
#include <cstring>
#include <stdexcept>
#include "../header/clientInfo.h"
#include "../header/client.h"
#include "../header/exitError.h"
#include "../header/msg.h"
#include "../header/pageDownloader.h"
#include "../header/fileSender.h"
using namespace std;
ClientInfo cInfo;
void parseArguments(int argc, char** argv)
{
    if (argc < 5 || argc > 7)
        exitError("Invalid number of arguments");
    for (int i = 1; i < argc; i++)
    {
        if (strcmp(argv[i], "-o") == 0)
        {
            if (i == argc - 1)
                exitError("Logfile unspecified");
            cout << "Logfile " << argv[i+1] << "\n";
            cInfo.logging = true;
            cInfo.logFileName = string(argv[i+1]) + "-" + to_string(getpid());
            cInfo.logFileOut = fopen((cInfo.logFileName + ".stdout").c_str(), "wt");
            cInfo.logFileErr = fopen((cInfo.logFileName + ".stderr").c_str(), "wt");
            i++;
        }
        else
            if (strcmp(argv[i], "-a") == 0)
            {
                if (i == argc - 1)
                    exitError("IP Address unspecified");
                cout << "IP Address " << argv[i+1] << "\n";
                cInfo.ipServer = argv[i+1];
                i++;
            }
            else
                if (strcmp(argv[i], "-p") == 0)
                {
                    if (i == argc - 1)
                        exitError("Unspecified port");
                    cout << "Port " << argv[i+1] << "\n";
                    try{
                        cInfo.portServer = stoi(argv[i+1]);
                    } catch (const invalid_argument& e)
                    {
                        exitError("Port argument is not an integer");
                    }
                    i++;
                }
                else
                    exitError("Invalid option");
    }

}
void openServerSocket()
{
    cInfo.serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (cInfo.serverSocket < 1)
    {
        if (cInfo.logging)
            fprintf(cInfo.logFileErr, "Error open server socket\n");
        exitError("Error open server socket");
    }
}
void setServerAddress()
{
    memset((char*)&cInfo.serverAddr, 0, sizeof(cInfo.serverAddr));
    cInfo.serverAddr.sin_family = AF_INET;
    cInfo.serverAddr.sin_port = htons(cInfo.portServer);
    inet_aton(cInfo.ipServer.c_str(), &cInfo.serverAddr.sin_addr);
}

void connectToServer()
{
    if (connect(cInfo.serverSocket, (struct sockaddr*)&cInfo.serverAddr, sizeof(cInfo.serverAddr)) < 0)
    {
        if (cInfo.logging)
            fprintf(cInfo.logFileErr, "Error connect\n");
        close(cInfo.serverSocket);
        exitError("Error connect");
    
    }

}
void setDescriptors()
{
    FD_ZERO(&cInfo.read_fds);
    FD_ZERO(&cInfo.tmp_fds);
    FD_SET(cInfo.serverSocket, &cInfo.read_fds);
}
void initClientInfo()
{
    cInfo.logging = false;
    cInfo.running = true;
}
void startClient()
{
    openServerSocket();
    setServerAddress();
    connectToServer();
}
void manageRequest()
{
    msg m;
    memcpy(&m, cInfo.buffer, sizeof(msg));
    if (m.type == SHUTDOWN)
    {
        cInfo.running = false;
        cout << "Server shutdown..\n";
        if (cInfo.logging)
            fprintf(cInfo.logFileOut, "Server shutdown..\n");
    }
    else
    {
        if (m.type == LINK)
        {
            string filePath, tmpFileName, domain;
            char depth;
            bool recursive = false, everything = false;
            downloadPage(cInfo, m, filePath, tmpFileName, domain, depth, recursive, everything);
            sendFile(cInfo, filePath, tmpFileName, domain, depth, recursive, everything);
        }
        else
        {

        }

    }

}
void runClient()
{
    setDescriptors();
    while (cInfo.running)
    {
        cInfo.tmp_fds = cInfo.read_fds;
        if (select(cInfo.serverSocket + 1, &cInfo.tmp_fds, NULL, NULL, NULL) == -1)
        {
            if (cInfo.logging)
                fprintf(cInfo.logFileErr, "Error select\n");
            close(cInfo.serverSocket);
            exitError("Error select");
        }
        if (FD_ISSET(cInfo.serverSocket, &cInfo.tmp_fds))
        {
            //response from server
            int received;
            if ((received = recv(cInfo.serverSocket, cInfo.buffer, BUFLEN, 0)) <= 0)
            {
                if (received == 0)
                {
                    printf("Server disconnected\n");
                    if (cInfo.logging)
                        fprintf(cInfo.logFileOut, "Server disconnected\n");
                    break;
                }
                else
                {
                    close(cInfo.serverSocket);
                    if (cInfo.logging)
                        fprintf(cInfo.logFileErr, "Error recv\n");
                    exitError("Error recv");
                }
            }
            else
            {
                manageRequest();
            }
        }
    }

}
void closeClient()
{
    close(cInfo.serverSocket);
    if (cInfo.logging)
    {
        fclose(cInfo.logFileOut);
        fclose(cInfo.logFileErr);
    }
}
int main(int argc, char** argv)
{
    initClientInfo();
    parseArguments(argc, argv);
    startClient();
    runClient();
    closeClient();
    
    return 0;

}
