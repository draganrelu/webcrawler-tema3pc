#include "../header/serverCommandExecutor.h"
#include "../header/exitError.h"
#include "../header/msg.h"
#include <iostream>
#include <cstring>
using namespace std;
void executeCommand(ServerInfo& sInfo)
{
    char command[BUFLEN];
    strcpy(command, sInfo.buffer);
    char* cmdName = strtok(command, " \n");
    if (!cmdName)
        exitError("Unknown command");
    if (strcmp(cmdName, "status") == 0)
        executeStatus(sInfo);
    else
    if (strcmp(cmdName, "download") == 0)
        executeDownload(sInfo);
    else
    if (strcmp(cmdName, "exit") == 0)
        executeExit(sInfo);
    else
        exitError("Unknown command");

}

void executeStatus(ServerInfo& sInfo)
{
    if (sInfo.logging)
        fprintf(sInfo.logFileOut, "Connected clients %d: ", (int)sInfo.connectedClients.size());
    else
        cout << "Connected clients " << sInfo.connectedClients.size() << ": ";
    for (auto it = sInfo.connectedClients.begin(); it != sInfo.connectedClients.end(); ++it)
    {

        if (sInfo.logging)
            fprintf(sInfo.logFileOut, "[socket:%d port:%d] ", it->first, it->second);
        else
            cout << "[socket:" << it->first << " port:" << it->second << "] ";
    }
    if (sInfo.logging)
        fprintf(sInfo.logFileOut, "\n");
    else
        cout << "\n";

}

void executeDownload(ServerInfo& sInfo)
{
    if (sInfo.logging)
        fprintf(sInfo.logFileOut, "Download put in queue..\n");
    else
        cout << "Download put in queue..\n";
    char* link = strtok(NULL, " \n");
    sInfo.queuedLinks.push(make_pair(link, 1));
}

void executeExit(ServerInfo& sInfo)
{
    msg m;
    m.type = SHUTDOWN;
    m.len = 0;
    for (auto it = sInfo.connectedClients.begin(); it != sInfo.connectedClients.end(); ++it)
    {
        sendMsg(&m, it->first);
    }
    sInfo.running = false;
    if (sInfo.logging)
        fprintf(sInfo.logFileOut, "Server is shutting down..\n");
    else
        cout << "Server is shutting down..\n";
}

