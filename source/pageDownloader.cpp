#include "../header/pageDownloader.h"
#include "../header/exitError.h"
#include <string>
#include <iostream>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <netdb.h>

#define HTTP_PORT 80
#define PACKSZ 250000
using namespace std;



void downloadPage(const ClientInfo& cInfo, const msg& m, string& filePath, string& tmpFileName, string& domain, char& depth, bool& recursive, bool& everything)
{
    static unsigned int tmpId;
    tmpId++;

    //get options from payload
    char rec = 0, ev = 0;
    memcpy(&depth, m.payload, sizeof(char));
    memcpy(&rec, m.payload+1, sizeof(char));
    memcpy(&ev, m.payload + 2, sizeof(char));
    if (rec != 0)
        recursive = true;
    if (ev != 0)
        everything = true;

    if (cInfo.logging)
        fprintf(cInfo.logFileOut, "Download request for %s with options %d%d and depth %d\n", m.payload + 3, (int)rec, (int)ev, (int)depth);
    else
        cout << "Download request for " << m.payload + 3<< " with options " << (int)rec << (int)ev << " depth " <<(int)depth<<   "\n";
    const char* link = m.payload + 3;

    //split link
    string hostname;
    getTokens(link, hostname, domain, filePath); 

    //open tmp file
    tmpFileName = "tmp_" + to_string((long long)getpid()) + "_" + to_string(tmpId);
    FILE* tmpfile = fopen(tmpFileName.c_str(), "wb");
    if (!tmpfile)
    {
        if(cInfo.logging)
            fprintf(cInfo.logFileErr, "Tmp file could not be opened\n");
        exitError("Tmp file cannot be opened");
    }

    int serverSocket = connectToHttpServer(cInfo, hostname);
    sendGetCommand(cInfo, serverSocket, filePath);
    getFile(cInfo, serverSocket, tmpfile);
}
void getTokens(const char* link, string& hostname, string& curPath, string& filePath)
{
    string slink = link;
    slink = slink.substr(slink.find("//") + 2);
    hostname = slink.substr(0, slink.find('/'));
    filePath = slink.substr(slink.find('/') + 1);
    slink = slink.substr(slink.find('/') + 1);
    size_t slashPos;
    while((slashPos = slink.find('/')) != string::npos)
    {
        curPath += slink.substr(0, slashPos + 1);
        slink = slink.substr(slashPos + 1);
    }
    curPath = hostname + "/" + curPath;
    //cout << "Hostname " << hostname << " path " << filePath << " curPath " << curPath << "\n";

}
int connectToHttpServer(const ClientInfo& cInfo, const string& domain)
{

    struct hostent *host = gethostbyname(domain.c_str());
    if (!host || !(host->h_addr_list[0]))
    {
        if (cInfo.logging)
            fprintf(cInfo.logFileErr, "Unknown hostname %s\n", domain.c_str());
        exitError("Unknown site");
    }

    int serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    if (serverSocket < 0)
    {
        if (cInfo.logging)
            fprintf(cInfo.logFileErr, "Error opening http server socket\n");
        exitError("Error open http server socket");
    }

    char ipAddr[1000];
    strcpy(ipAddr, inet_ntoa(*(struct in_addr*)(host->h_addr_list[0])));

    struct sockaddr_in addr;
    memset((char*)&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(HTTP_PORT);
    inet_aton(ipAddr, &addr.sin_addr);

    if (connect(serverSocket, (struct sockaddr*)&addr, sizeof(addr)) < 0)
    {
        if (cInfo.logging)
            fprintf(cInfo.logFileErr, "Error connecting to http server\n");
        exitError("Error connect to http server\n");
    }
    return serverSocket;
}

void sendGetCommand(const ClientInfo& cInfo, int serverSocket, const string& filePath)
{

    char command[1000];
    sprintf(command, "GET /%s HTTP/1.0 \r\n\r\n", filePath.c_str());

    int sent = send(serverSocket, command, strlen(command), 0);
    if (sent < 0)
    {
        if (cInfo.logging)
            fprintf(cInfo.logFileErr, "Erorr sending get request\n");
        exitError("Error sending get request\n");
    }
}
void getFile(const ClientInfo& cInfo, int serverSocket, FILE* tmpfile)
{

    char buf[PACKSZ];
    while(1)
    {
        int primit;
        if ((primit = recv(serverSocket, buf, PACKSZ, 0)) <= 0)
        {
            if (primit < 0)
            {
                if (cInfo.logging)
                    fprintf(cInfo.logFileErr, "Error receiving from http server\n");
                exitError("Error receiving from http server");
            }
            break;
        }

        fwrite(buf, primit, sizeof(char), tmpfile);
    }
    
    fclose(tmpfile);
}
