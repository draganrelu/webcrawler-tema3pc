#include <iostream>
#include <cstring>
#include <stdexcept>
#include <sys/stat.h>
#include <unistd.h>
#include "../header/serverInfo.h"
#include "../header/server.h"
#include "../header/serverCommandExecutor.h"
#include "../header/exitError.h"
#include "../header/msg.h"
#include "../header/fileReceiver.h"
using namespace std;
ServerInfo sInfo;
void parseArguments(int argc, char** argv)
{
    if (argc > 7 || argc < 3)
        exitError("Invalid number of arguments");

    for (int i = 1; i < argc; i++)
    {
        if (strcmp(argv[i], "-r") == 0)
        {
            //recursive
            sInfo.recursive = true;
            cout << "Recursive\n";
        }
        else
            if (strcmp(argv[i], "-e") == 0)
            {
                //everything
                sInfo.everything = true;
                cout << "Everything\n";
            }
            else
                if (strcmp(argv[i], "-o") == 0)
                {
                    //fisier log
                    if (i == argc - 1)
                        exitError("Logfile name unspecified");
                    sInfo.logFileName = argv[i+1];
                    sInfo.logFileOut = fopen((sInfo.logFileName + ".stdout").c_str(), "wt");
                    sInfo.logFileErr = fopen((sInfo.logFileName + ".stderr").c_str(), "wt");
                    sInfo.logging = true;
                    cout << "Logfile " << argv[i+1] << "\n";
                    i++;
                }
                else
                    if (strcmp(argv[i], "-p") == 0)
                    {
                        if (i == argc - 1)
                            exitError("Port unspecified");
                        try{
                            sInfo.port = stoi(argv[i+1]);
                        } catch (const invalid_argument& e)
                        {
                            exitError("Port argument is not an integer");
                        }
                        
                        cout << "Port " << argv[i+1] << "\n";
                        i++;
                    }
                    else
                        exitError("Invalid option");

    }
}
void initServerInfo()
{
    FD_ZERO(&sInfo.read_fds);
    FD_ZERO(&sInfo.tmp_fds);
    sInfo.recursive = false;
    sInfo.everything = false;
    sInfo.logging = false;
    sInfo.running = true;
}
void openServerSocket()
{
    sInfo.serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (sInfo.serverSocket < 0)
    {
        if (sInfo.logging)
            fprintf(sInfo.logFileErr, "Open socket error\n");
        exitError("Open socket error");
    }

    if (sInfo.logging)
        fprintf(sInfo.logFileOut, "Server opened on socket %d\n", sInfo.serverSocket);
    else
        cout << "Server opened on socket " << sInfo.serverSocket << "\n";
}
void setAddress()
{
    memset((char*)&sInfo.addr, 0, sizeof(sInfo.addr));
    sInfo.addr.sin_family = AF_INET;
    sInfo.addr.sin_port = htons(sInfo.port);
    inet_aton("127.0.0.1", &(sInfo.addr.sin_addr));
}
void bindAddress()
{
    if (bind(sInfo.serverSocket, (struct sockaddr*)&sInfo.addr, sizeof(sInfo.addr)) < 0)
    {
        if (sInfo.logging)
            fprintf(sInfo.logFileErr, "Bind address error\n");
        exitError("Bind address error");
    }
}
void listenClients()
{
    if (listen(sInfo.serverSocket, MAX_CONN_CLIENTS) < 0)
    {
        close(sInfo.serverSocket);
        if (sInfo.logging)
            fprintf(sInfo.logFileErr, "Error listening to clients\n");
        exitError("Error listening to clients");
    }
}
void startServer()
{
    openServerSocket();
    setAddress();
    bindAddress();
    listenClients();
}
void setDescriptors()
{
    FD_SET(sInfo.serverSocket, &sInfo.read_fds);
    FD_SET(0, &sInfo.read_fds);
    sInfo.fdmax = sInfo.serverSocket; 
}
void tryToAssignLinks()
{

    if (sInfo.connectedClients.size() >= MIN_CONN_CLIENTS)
        
        for (auto ic = sInfo.idleClients.begin(); ic != sInfo.idleClients.end();)
        {
            if (sInfo.queuedLinks.size() > 0)
            {
                //send link to be downloaded to client
                //payload format: depth + recursive + everything + link name
                pair<string,char> link = sInfo.queuedLinks.front();
                sInfo.queuedLinks.pop();
                msg m;
    

                //set option flags
                char recursive = 0, everything = 0;
                if (link.second < MAX_LINK_DEPTH)
                {
                    if (sInfo.recursive)
                        recursive = 1;
                    if (sInfo.everything)
                        everything = 1;
                }

                //send command
                memcpy(m.payload, &link.second, sizeof(char));
                memcpy(m.payload + 1, &recursive, sizeof(char));
                memcpy(m.payload + 2, &everything, sizeof(char));
                strcpy(m.payload + 3, link.first.c_str());
                m.len = strlen(m.payload) + 4;
                m.type = LINK;

                if (sInfo.logging)
                    fprintf(sInfo.logFileOut, "Assigned %s to client %d\n", link.first.c_str(), ic->second);

                sendMsg(&m, ic->second); 
                ic = sInfo.idleClients.erase(ic);
            }
            else
                return;

        }
}
void receiveLink(msg& m)
{
    char depth;
    memcpy(&depth, m.payload, sizeof(char));
    string link = m.payload + 1;

    if (sInfo.logging)
        fprintf(sInfo.logFileOut, "Received link %s with depth %d\n", m.payload + 1, (int)depth);
    else 
        printf("Received link %s with depth %d\n", m.payload + 1, (int)depth);
    sInfo.queuedLinks.push(make_pair(link, depth));
}
void manageCallback(msg& m, int client)
{


        if (sInfo.logging)
            fprintf(sInfo.logFileOut, "Client %d finished downloading %s and is idle now\n", client, m.payload);
        else
            printf("Client %d finished downloading %s and is idle now\n", client, m.payload);
        sInfo.jobsDone[client]++;
        sInfo.idleClients.insert(make_pair(sInfo.jobsDone[client], client));
}
void manageResponse(int client)
{
    msg m;
    memcpy(&m, sInfo.buffer, sizeof(msg));
    if (m.type == CALLBACK)
    {
        manageCallback(m, client);
    }
    else
    {
        if (m.type == FILEPACK)
        {
            receiveFile(m);
        }
        else
            if (m.type == LINK)
            {
                receiveLink(m);
            }

    }


}
void runServer()
{
    setDescriptors();
    while (sInfo.running)
    {
        sInfo.tmp_fds = sInfo.read_fds;
        if (select(sInfo.fdmax+1, &sInfo.tmp_fds, NULL, NULL, NULL) == -1)
        {
            if (sInfo.logging)
                fprintf(sInfo.logFileErr, "Error select\n");
            close(sInfo.serverSocket);
            exitError("Error select\n");
        }

        int i;
        for (i = 0; i <= sInfo.fdmax; i++)
        {
            if (FD_ISSET(i, &sInfo.tmp_fds))
            {
                if (i == 0)
                {
                    //citesc de la tastatura o comanda
                    fgets(sInfo.buffer, BUFLEN, stdin);
                    executeCommand(sInfo);

                }
                else
                    if (i == sInfo.serverSocket)
                    {
                        //se conecteaza un nou client
                        int clientSocket;
                        struct sockaddr_in clientAddr;
                        socklen_t clientAddrLen;
                        if ((clientSocket = accept(sInfo.serverSocket, (struct sockaddr*)&clientAddr, &clientAddrLen)) == -1)
                        {
                            close(sInfo.serverSocket);
                            if(sInfo.logging)
                                fprintf(sInfo.logFileErr, "Error accept\n");
                            exitError("Error accept\n");
                        }
                        else
                        {
                            sInfo.connectedClients[clientSocket] = ntohs(clientAddr.sin_port);

                            if (sInfo.logging)
                                fprintf(sInfo.logFileOut, "New connection from %s on port %d and socket %d\n", 
                                        inet_ntoa(clientAddr.sin_addr), ntohs(clientAddr.sin_port), clientSocket); 
                            else
                                cout << "New connection from " << inet_ntoa(clientAddr.sin_addr) << " on port " 
                                    << ntohs(clientAddr.sin_port) << " and socket " << clientSocket << "\n";
                            if (clientSocket > sInfo.fdmax)
                                sInfo.fdmax = clientSocket;
                            FD_SET(clientSocket, &sInfo.read_fds);
                            sInfo.idleClients.insert(make_pair(0,clientSocket));
                            sInfo.jobsDone[clientSocket] = 0;
                        }

                    }
                    else
                        //primesc date de la un client
                    {
                        memset(sInfo.buffer, 0, BUFLEN);
                        int received;
                        if ((received = recv(i, sInfo.buffer, sizeof(sInfo.buffer), 0)) <= 0)
                        {
                            if (received == 0)
                            {
                                //s-a deconectat un client

                                if (sInfo.logging)
                                    fprintf(sInfo.logFileOut, "Client %d disconnected\n", i);
                                else
                                    cout << "Client " << i <<" disconnected\n";
                                sInfo.connectedClients.erase(i);
                                close(i);
                                FD_CLR(i, &sInfo.read_fds);
                            }
                            else
                            {
                                close(sInfo.serverSocket);
                                if (sInfo.logging)
                                    fprintf(sInfo.logFileErr, "Recv error from client %d\n", i);
                                exitError("Recv error");

                            }

                        }
                        else
                        {
                            manageResponse(i);
                        }
                        


                    }

            }
        }
        tryToAssignLinks();


        

    }

}
void closeServer()
{
    close(sInfo.serverSocket);
    if (sInfo.logging)
    {
        fclose(sInfo.logFileOut);
        fclose(sInfo.logFileErr);
    }
}
int main(int argc, char** argv)
{
    initServerInfo();
    parseArguments(argc, argv);
    startServer();
    runServer();
    closeServer();
    return 0;
}
