#include "../header/fileSender.h"
#include "../header/msg.h"
#include "../header/constants.h"
#include <string.h>
#include <iostream>
#include <fstream>

#include <unistd.h>
using namespace std;
void sendFile(ClientInfo& cInfo, const string& filePath, const string& tmpFileName, const string& domain, const char& depth, const bool& recursive, const bool& everything)
{

    if (cInfo.logging)
        fprintf(cInfo.logFileOut, "Sending file %s with options %d%d\n", filePath.c_str(), (int)recursive, (int)everything);
    else
        printf("Sending file %s with options %d%d.\n",filePath.c_str(),(int) recursive, (int)everything);
    FILE* f = fopen(tmpFileName.c_str(), "rb");
    readHeader(f);
    int citit;
    char buf[FPACKSZ];
    msg m;
    //pack is filePath + nrocts + octs
    while ((citit = fread(buf, sizeof(char), FPACKSZ, f)) > 0)
    {
        strcpy(m.payload, filePath.c_str());
        memcpy(m.payload + strlen(filePath.c_str()) + 1, &citit, sizeof(int));
        memcpy(m.payload + strlen(filePath.c_str()) + 5, buf, citit);
        m.len = citit;
        m.type = FILEPACK;
        sendMsg(&m, cInfo.serverSocket);
    }
    fclose(f);

    //send links
    if (recursive)
        sendLinksInPage(cInfo, tmpFileName, domain, depth, everything);

    m.type = CALLBACK;
    strcpy(m.payload, filePath.c_str());
    sendMsg(&m, cInfo.serverSocket);
    unlink(tmpFileName.c_str());
}
void sendLinksInPage(const ClientInfo& cInfo, const string& tmpFileName, const string& domain, const char& depth, const bool& everything)
{

    if (cInfo.logging)
        fprintf(cInfo.logFileOut, "Sending links..\n");
    else
        printf("Sending links..\n");

    vector<string> files;
    getFiles(files, tmpFileName, everything);
    sendLinks(cInfo, domain, depth, files);
}
void getFiles(vector<string>& files, const string& tmpFileName, const bool& everything)
{
    string line;
    ifstream tmpf(tmpFileName);
    while (getline(tmpf, line))
    {
        getFiles(files, everything, line);
    }
    tmpf.close();

}
void getFiles(vector<string>& files, const bool& everything, const string& line)
{
    size_t linkPos;
    string mline = line;
    string filePath;

    while ((linkPos = mline.find("href=\"")) != string::npos)
    {
        mline = mline.substr(linkPos + 6);
        filePath = mline.substr(0, mline.find("\""));
        mline = mline.substr(mline.find("\"") + 1);
        if (!everything)
        {
            if (checkLinkOrigin(filePath) && checkHtmlLink(filePath))
                files.push_back(filePath);
        }
        else
            if (checkLinkOrigin(filePath) && (checkIsFile(filePath) || (checkHtmlLink(filePath))))
            {
                files.push_back(filePath);
            }    
    }

}
bool checkIsFile(const string& link)
{
    if (link.size() > 4)
    {
        string ext = link.substr(link.size() - 4, 4);
        if (ext[0] == '.')
            return true;
    }
    if (link.size() > 3)
    {
        string ext = link.substr(link.size() - 3, 3);
        if (ext[0] == '.')
            return true;
    }
    return false;
}

bool checkLinkOrigin(const string& link)
{
    if (link.size() > 7)
        if (strcmp(link.substr(0, 7).c_str(), "http://") == 0)
            return false;
    if (link.size() > 8)
        if (strcmp(link.substr(0, 8).c_str(), "https://") == 0)
            return false;
    if (link.size() > 1)
        if (link.find('#') != string::npos)
            return false;
    return true;
}
bool checkHtmlLink(const string& link)
{
    if (link.size() > 5)
        if (strcmp(link.substr(link.size() - 5).c_str(), ".html") == 0)
            return true;
    if (link.size() > 4)
        if (strcmp(link.substr(link.size() - 4).c_str(), ".htm") == 0)
            return true;
    return false;

}
void sendLinks(const ClientInfo& cInfo, const string& domain, const char& depth, const vector<string>& links)
{
    msg m;
    for (int i = 0; i < (int)links.size(); i++)
    {

        m.type = LINK;
        sprintf(m.payload, "%chttp://%s%s", depth + 1, domain.c_str(), links[i].c_str());
        m.len = strlen(m.payload + 1) + 2;

        if (cInfo.logging)
            fprintf(cInfo.logFileOut, "Sending link %s\n", m.payload + 1);
        else
            printf("Sending link %s\n", m.payload + 1);
        sendMsg(&m, cInfo.serverSocket);
    }
}
void readHeader(FILE* f)
{
    char chars[5] = {0};
    do
    {
        chars[0] = chars[1];
        chars[1] = chars[2];
        chars[2] = chars[3];
        fread(&chars[3], 1, sizeof(char), f);
        if (strcmp(chars, "\r\n\r\n") == 0)
            break;
    } while(chars[3] != EOF);
}


