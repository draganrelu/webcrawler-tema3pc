#include "../header/fileReceiver.h"   
#include "../header/exitError.h"
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

using namespace std;
int openFile(const string& path)
{
    
    size_t slashPos;
    string cpPath = path;
    string dirpath;
    struct stat st = {0};
    if (stat(path.c_str(), &st) == -1)
    {
        while ((slashPos = cpPath.find('/')) != string::npos)
        {
            dirpath += cpPath.substr(0, slashPos+1);
            if (stat(dirpath.c_str(), &st) == -1)
                mkdir(dirpath.c_str(), 0777);
            cpPath = cpPath.substr(slashPos + 1);
        }
    }
    int fd = open(path.c_str(), O_WRONLY | O_CREAT | O_APPEND, 0644);
    return fd;


}
void receiveFile(msg& m)
{

    char* filePath = m.payload;
   // printf("received pack for file %s\n", filePath);
    int fd = openFile(filePath);
    if (fd < 0)
        exitError("Could not open file");
    int nrOcts;
    memcpy(&nrOcts, m.payload + strlen(filePath) + 1, sizeof(int));
    write(fd, m.payload + strlen(filePath) + 5, nrOcts);
    close(fd);
}
